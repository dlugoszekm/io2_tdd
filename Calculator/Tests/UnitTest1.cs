using System;
using Xunit;

namespace Tests
{
    public class UnitTest1
    {
        [Fact]
        public void EmptyString()
        {
            string input = "";

            Assert.Equal(0, Calculator.Calculator.Calculate(input));
        }


        [Fact]
        public void SingleNumber()
        {
            string input = "5";

            Assert.Equal(5, Calculator.Calculator.Calculate(input));
        }

        [Theory]
        [InlineData('\n')]
        [InlineData(',')]
        public void DelimSumming(char delim)
        {
            string input = "5" + delim + "100";
            Assert.Equal(105, Calculator.Calculator.Calculate(input));
        }

        [Fact]
        public void MultiDelimSumming()
        {
            string input = "1,1\n1,1\n1";
            Assert.Equal(5, Calculator.Calculator.Calculate(input));
        }

        [Fact]
        public void Negative()
        {
            string input = "-100";
            Assert.Throws(typeof(Exception),() => Calculator.Calculator.Calculate(input));
        }

        [Fact]
        public void GreaterThan1000Ingored()
        {
            string input = "1,1\n1,100000000,1\n1";
            Assert.Equal(5, Calculator.Calculator.Calculate(input));
        }


        [Fact]
        public void SingleCharCustomDelim()
        {
            string input = "//#1,1\n1,100000000#1#1";
            Assert.Equal(5, Calculator.Calculator.Calculate(input));
        }


        [Fact]
        public void MultiCharCustomDelim()
        {
            string input = "//[###]1,1\n1,100000000###1###1";
            Assert.Equal(5, Calculator.Calculator.Calculate(input));
        }


        [Fact]
        public void ManyMultiCharCustomDelims()
        {
            string input = "//[###][!][??]1??1!1,###1###1,1";
            Assert.Equal(6, Calculator.Calculator.Calculate(input));
        }
    }
}