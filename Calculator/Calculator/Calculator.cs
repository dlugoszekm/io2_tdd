﻿using System;
using System.Collections.Generic;

namespace Calculator
{
    public class Calculator
    {
        public static int Calculate(string expression)
        {
            return calculateRecursive(expression, new List<string>{ "\n", "," });
        }
       
        private static int calculateRecursive(string expression, List<string> delims)
        {
            if (expression.Length == 0)
                return 0;

            if (int.TryParse(expression, out int val))
            {
                if (val < 0)
                {
                    throw new Exception("negative");
                }
                if (val > 1000)
                {
                    return 0;
                }
                return val;
            }
            
            if (expression.StartsWith("//"))
            {
                if(expression[2] == '[')
                {
                    int toStart = 4;
                    int delimBegin = 3;
                    for(int i = delimBegin+1; i<expression.Length;i++)
                    {
                        if(expression[i] == '[')
                        {
                            delimBegin = i + 1;
                        }
                        if(expression[i] == ']')
                        {
                            toStart = i + 1;
                            delims.Add(expression.Substring(delimBegin, i - delimBegin));
                        }
                    }
                    expression = expression.Substring(toStart);
                } else {
                    delims.Add(expression[2].ToString());
                    expression = expression.Substring(3);
                }
            }

            foreach (string delim in delims)
            {
                if (expression.Contains(delim))
                {
                    var splitIndex = expression.IndexOf(delim);
                    return 
                        calculateRecursive(expression.Substring(0, splitIndex), delims) +
                        calculateRecursive(expression.Substring(splitIndex + delim.Length), delims);
                }
            }

            return -1;
        }
    }
}
